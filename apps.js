const express = require('express');
const app = express();
const port = 8000;

// habilitar servidor

app.listen(port,()=>{
    console.log("Projeto executando na porta: "+ port);
});

// recurso de request.query
app.get('/aluno/filtros',(req,res)=>{
    let source= req.query;
    let ret = "Dados solicitados: "+ source.nome + " " + source.sobrenome;
    res.send("{message: aluno encontrado}");
});